﻿using UnityEngine;
using System.Collections;

public class Questions : MonoBehaviour {

    public static int noOfQuestions = 5;

    public static string[] question1 = { "Studies show that eating a healthy breakfast can help give you:", "A more nutritionally complete diet, higher in nutrients, vitamins and minerals", "Improved concentration and performance in the classroom or the boardroom", "More strength and endurance to engage in", "All the above and more", "4" };
    public static string[] question2 = { "Total daily calorie intake which you should take from breakfast should be around", "5% - 10%", "10% - 12%", "15% - 19%", "20 - 25%", "4" };
    public static string[] question3 = { "Breakfast helps your kid to", "Think better", "To avoid eating junk food before lunch", "Have more energy to burn", "All of the above & more", "4" };
    public static string[] question4 = { "Component of a healthy breakfast?", "Fruits only", "Protein  , healthy fats & fruits", "Whole grain, protein, healthy fats & fruit", "Whole grain, protein  & healthy fats", "3" };
    public static string[] question5 = { "What will happen if your kid does not have a healthy breakfast?", "They do not have enough energy to kick start the day", "Poor attention & memory during their studies", "Tend to eat more junk food", " All of the above & more", "4" };

    public static string[][] question = { question1, question2, question3, question4, question5};

	
}
