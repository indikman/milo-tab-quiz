﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;
using DG.Tweening;

public class GameController : MonoBehaviour {

    [Header("Main Panels")]
    public GameObject StartPanel;
    public GameObject GamePanel;
    public GameObject ResultPanel;

    [Header("Game Panel")]
    public GameObject btn1;
    public GameObject btn2;
    public GameObject btn3;
    public GameObject btn4;
    public Text btn_text1;
    public Text btn_text2;
    public Text btn_text3;
    public Text btn_text4;

    public Text question;
    public Text QuestionNumber;
    public GameObject shadowLeft;
    public GameObject shadowRight;

    [Header("Result Panel")]
    public GameObject CorrectText;
    public GameObject OopsText;
    public Text CorrectAnswerText;
    public Text ScoreText;
    public GameObject CupOK;
    public GameObject CupOops;
    public GameObject ButtonNext;
    public GameObject Card;
    public GameObject yougotText;
    public GameObject answerCorrectText;

    public static int score = 0;
    private int currentQuestion;

	// Use this for initialization
	void Start () {
        score = 0;
        currentQuestion = 0;

        //Enable only start Panel
        GamePanel.SetActive(false);
        ResultPanel.SetActive(false);
        StartPanel.SetActive(true);

        //Animations
        DOTween.Init();
	}
	

    public void LoadQuestion(int questionNumber) {

        currentQuestion = questionNumber;

        question.text = Questions.question[questionNumber - 1][0];
        btn_text1.text = Questions.question[questionNumber - 1][1];
        btn_text2.text = Questions.question[questionNumber - 1][2];
        btn_text3.text = Questions.question[questionNumber - 1][3];
        btn_text4.text = Questions.question[questionNumber - 1][4];
        QuestionNumber.text = "Question " + questionNumber;

        StartPanel.SetActive(false);
        ResultPanel.SetActive(false);
        GamePanel.SetActive(true);


        //Animation
        QuestionNumber.transform.DOMoveY(500.0f, 1.0f).SetEase(Ease.OutBack).From();

        question.transform.DOMoveX(-1500.0f, 1.0f).SetEase(Ease.OutBack).SetDelay(0.2f).From();
        question.GetComponent<Text>().DOColor(new Color(255, 255, 255, 0), 1.0f).SetDelay(0.2f).From();

        btn1.transform.DOScale(0.5f, 0.5f).From().SetDelay(1.0f).SetEase(Ease.OutBack);
        btn2.transform.DOScale(0.5f, 0.5f).From().SetDelay(1.2f).SetEase(Ease.OutBack);
        btn3.transform.DOScale(0.5f, 0.5f).From().SetDelay(1.4f).SetEase(Ease.OutBack);
        btn4.transform.DOScale(0.5f, 0.5f).From().SetDelay(1.6f).SetEase(Ease.OutBack);


        btn1.transform.DORotate(new Vector3(0.0f, 90.0f, 0.0f), 0.5f).SetDelay(1.0f).From();
        btn2.transform.DORotate(new Vector3(0.0f, 90.0f, 0.0f), 0.5f).SetDelay(1.2f).From();
        btn3.transform.DORotate(new Vector3(0.0f, 90.0f, 0.0f), 0.5f).SetDelay(1.4f).From();
        btn4.transform.DORotate(new Vector3(0.0f, 90.0f, 0.0f), 0.5f).SetDelay(1.6f).From();
        

        btn1.GetComponent<Image>().DOColor(new Color(255, 255, 255, 0), 0.5f).SetDelay(1.0f).From();
        btn2.GetComponent<Image>().DOColor(new Color(255, 255, 255, 0), 0.5f).SetDelay(1.2f).From();
        btn3.GetComponent<Image>().DOColor(new Color(255, 255, 255, 0), 0.5f).SetDelay(1.4f).From();
        btn4.GetComponent<Image>().DOColor(new Color(255, 255, 255, 0), 0.5f).SetDelay(1.6f).From();

        shadowLeft.transform.DORotate(new Vector3(0.0f, 90.0f, 0.0f), 0.5f).SetDelay(1.3f).From();
        shadowLeft.GetComponent<Image>().DOColor(new Color(255, 255, 255, 0), 0.5f).SetDelay(1.3f).From();
        shadowRight.transform.DORotate(new Vector3(0.0f, 90.0f, 0.0f), 0.5f).SetDelay(1.3f).From();
        shadowRight.GetComponent<Image>().DOColor(new Color(255, 255, 255, 0), 0.5f).SetDelay(1.3f).From();

    }

    public void submitAnswer(int answerNumber) {
        if (answerNumber == Int32.Parse(Questions.question[currentQuestion - 1][5]))
        {
            //Correct
            score++;
            loadCorrect();
        }
        else
        { 
            //wrong
            loadWrong();
        }
    }

    public void loadCorrect() {
        //GamePanel.SetActive(false);

        //------------------------
        CupOK.SetActive(true);
        CorrectText.SetActive(true);
        //------------------------
        OopsText.SetActive(false);
        CupOops.SetActive(false);
        CorrectAnswerText.gameObject.SetActive(false);
        //------------------------
        ScoreText.gameObject.SetActive(false);
        yougotText.SetActive(false);
        answerCorrectText.SetActive(false);

        //Animations
        ResultPanel.GetComponent<Image>().DOColor(new Color(255.0f, 255.0f, 255.0f, 0.0f), 1.0f).From().SetEase(Ease.OutQuad);
        Card.transform.DORotate(new Vector3(0.0f, 90.0f, 0.0f), 1.0f).SetEase(Ease.OutBack).SetDelay(0.3f).From();
        
        CupOK.transform.DOScale(0.0f, 1.0f).SetDelay(0.5f).SetEase(Ease.OutBack).From();
        CorrectText.transform.DOScale(0.0f, 1.0f).SetDelay(0.75f).SetEase(Ease.OutBack).From();

        ButtonNext.transform.DOScale(0.0f, 1.0f).SetDelay(0.5f).SetEase(Ease.OutExpo).From();


        ResultPanel.SetActive(true);
    }

    public void loadWrong()
    {
        //GamePanel.SetActive(false);

        //------------------------
        CupOK.SetActive(false);
        CorrectText.SetActive(false);
        //------------------------
        CorrectAnswerText.text = Questions.question[currentQuestion - 1][Int32.Parse(Questions.question[currentQuestion - 1][5])];

        OopsText.SetActive(true);
        CupOops.SetActive(true);
        CorrectAnswerText.gameObject.SetActive(true);
        //------------------------
        ScoreText.gameObject.SetActive(false);
        yougotText.SetActive(false);
        answerCorrectText.SetActive(false);

        //Animations

        ResultPanel.GetComponent<Image>().DOColor(new Color(255.0f, 255.0f, 255.0f, 0.0f), 1.0f).From().SetEase(Ease.OutQuad);
        Card.transform.DORotate(new Vector3(0.0f, 90.0f, 0.0f), 1.0f).SetEase(Ease.OutBack).SetDelay(0.3f).From();
        CupOops.transform.DOScale(0.0f, 1.0f).SetDelay(0.5f).SetEase(Ease.OutCubic).From();

        OopsText.transform.DOScale(0.0f, 1.0f).SetDelay(0.75f).SetEase(Ease.OutBack).From();
        CorrectAnswerText.transform.DOScale(0.0f, 1.0f).SetDelay(1.0f).SetEase(Ease.OutBack).From();

        ButtonNext.transform.DOScale(0.0f, 1.0f).SetDelay(1.0f).SetEase(Ease.OutBack).From();

        ResultPanel.SetActive(true);
    }

    public void loadFinish()
    {

        //------------------------
        CupOK.SetActive(false);
        CorrectText.SetActive(false);
        //------------------------
        OopsText.SetActive(false);
        CupOops.SetActive(true);
        CorrectAnswerText.gameObject.SetActive(false);
        //------------------------
        ScoreText.text = score + " / " + Questions.noOfQuestions;
        ScoreText.gameObject.SetActive(true);
        yougotText.SetActive(true);
        answerCorrectText.SetActive(true);

        Card.transform.DORotate(new Vector3(0.0f, 90.0f, 0.0f), 1.0f).SetEase(Ease.OutBack).SetDelay(0.3f).From();
        ButtonNext.transform.DOScale(0.0f, 1.0f).SetDelay(0.5f).SetEase(Ease.OutBack).From();
        CupOops.transform.DOScale(0.0f, 1.0f).SetEase(Ease.OutBack).From();


        yougotText.transform.DOScale(0.0f, 1.0f).SetDelay(0.5f).SetEase(Ease.OutBack).From();
        ScoreText.transform.DOScale(0.0f, 1.0f).SetDelay(0.7f).SetEase(Ease.OutBack).From();
        answerCorrectText.transform.DOScale(0.0f, 1.0f).SetDelay(0.9f).SetEase(Ease.OutBack).From();
    }

    public void loadNext() {
        if (currentQuestion == 99) {
            Application.LoadLevel("game");
        }
        else if (currentQuestion >= Questions.noOfQuestions)
        {
            //load finish
            loadFinish();
            currentQuestion = 99;
        }
        else
        {
            LoadQuestion(currentQuestion + 1);
        }
    }

    public void StartGame() {
        LoadQuestion(1);
    }
}
